package veceditor.Exception;

/**
 * Exception class that is thrown when the image size entered was invalid.
 * (When exporting to bmp)
 */
public class InvalidImageSizeException extends ErrorDialogException {

    /**
     * Creates a dialog and prompt a message.
     *
     * @param message message to show to the user
     * @param testing flag of whether a test is running or not
     */
    public InvalidImageSizeException(String message, boolean testing) {
        super(message, testing);
    }
}
