package veceditor.Exception;

/**
 * Exception class that is thrown when a color in a VEC file is invalid.
 */
public class InvalidColorException extends ErrorDialogException {

    /**
     * Creates a dialog and prompt a message.
     *
     * @param message message to show to the user
     * @param testing flag of whether a test is running or not
     */
    public InvalidColorException(String message, boolean testing) {
        super(message, testing);
    }
}
