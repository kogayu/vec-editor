package veceditor.Exception;

/**
 * Exception class that is thrown when a command in a VEC file is invalid.
 */
public class InvalidCommandException extends ErrorDialogException {

    /**
     * Creates a dialog and prompt a message.
     *
     * @param message message to show to the user
     * @param testing flag of whether a test is running or not
     */
    public InvalidCommandException(String message, boolean testing) {
        super(message, testing);
    }
}
