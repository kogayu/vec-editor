package veceditor.Exception;

import javax.swing.*;

class ErrorDialogException extends Exception {

    /**
     * Creates a dialog and prompt a message.
     *
     * @param message message to show to the user
     */
    ErrorDialogException(String message, boolean testing) {
        super(message);
        if (!testing) {
            JOptionPane.showMessageDialog(null, message, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }
}
