package veceditor.View;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * JMenuBar which is placed as top menu bar
 * Holds JMenuItems
 * Adds action listener to JMenuItem
 */
public class TopMenuBar extends JMenuBar {

    private JMenuItem newFile;
    private JMenuItem open;
    private JMenuItem save;
    private JMenuItem saveAs;
    private JMenuItem bmpExport;
    private JMenuItem undo;

    public TopMenuBar() {
        // Create Edit and File menu
        JMenu file = new JMenu("File");
        JMenu edit = new JMenu("Edit");
        add(file);
        add(edit);

        // Create buttons, and assign open and save buttons to file menu,
        // undo button to edit
        this.newFile = new JMenuItem("new");
        this.open = new JMenuItem("open");
        this.save = new JMenuItem("save");
        this.saveAs = new JMenuItem("save as..");
        this.bmpExport = new JMenuItem("export as bmp format");
        this.undo = new JMenuItem("undo");

        file.add(this.newFile);
        file.add(this.open);
        file.add(this.save);
        file.add(this.saveAs);
        file.add(this.bmpExport);
        edit.add(this.undo);

    }

    /**
     * Get JMenuItem with label "new"
     *
     * @return JMenuItem newFile
     */
    public JMenuItem getNewFile() {
        return newFile;
    }

    /**
     * Get JMenuItem with label "open"
     *
     * @return JMenuItem open
     */
    public JMenuItem getOpen() {
        return this.open;
    }

    /**
     * Get JMenuItem with label "save"
     *
     * @return JMenuItem save
     */
    public JMenuItem getSave() {
        return this.save;
    }

    /**
     * Get JMenuItem with label "save as.."
     *
     * @return JMenuItem saveAs
     */
    public JMenuItem getSaveAs() {
        return saveAs;
    }

    /**
     * Get JMenuItem with label "export to bmp"
     *
     * @return JMenuItem bmpExport
     */
    public JMenuItem getBmpExport() {
        return this.bmpExport;
    }

    /**
     * Get JMenuItem with label "undo"
     *
     * @return JMenuItem undo
     */
    public JMenuItem getUndo() {
        return this.undo;
    }

    public void addActionListener(ActionListener actionListener) {
        this.newFile.addActionListener(actionListener);
        this.open.addActionListener(actionListener);
        this.save.addActionListener(actionListener);
        this.saveAs.addActionListener(actionListener);
        this.bmpExport.addActionListener(actionListener);
        this.undo.addActionListener(actionListener);
    }

}
