package veceditor.View;

import veceditor.Model.ShapeConfig;
import veceditor.Model.ShapeType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * A Panel Class that holds all shape buttons.
 */
public class ShapeButtonsPanel extends JPanel {

    private ShapeButton plotButton;
    private ShapeButton lineButton;
    private ShapeButton rectangleButton;
    private ShapeButton ellipseButton;
    private ShapeButton polygonButton;

    /**
     * Gets the PLOT shape button.
     *
     * @return plotButton
     */
    public ShapeButton getPlotButton() {
        return plotButton;
    }

    /**
     * Gets the LINE shape button.
     *
     * @return lineButton
     */
    public ShapeButton getLineButton() {
        return lineButton;
    }

    /**
     * Gets the RECTANGLE shape button.
     *
     * @return rectangleButton
     */
    public ShapeButton getRectangleButton() {
        return rectangleButton;
    }

    /**
     * Gets the ELLIPSE shape button.
     *
     * @return ellipseButton
     */
    public ShapeButton getEllipseButton() {
        return ellipseButton;
    }

    /**
     * Gets the POLYGON shape button.
     *
     * @return polygonButton
     */
    public ShapeButton getPolygonButton() {
        return polygonButton;
    }

    public ShapeButtonsPanel(ShapeConfig shapeConfig) {
        super(new GridLayout(0, 2, 5, 5));
        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10), "Shapes"));

        Icon plotIcon = new ImageIcon(resizeIcon("../../images/plot.png"));
        this.plotButton = new ShapeButton(plotIcon, ShapeType.PLOT);
        add(this.plotButton);

        Icon lineIcon = new ImageIcon(resizeIcon("../../images/line.png"));
        this.lineButton = new ShapeButton(lineIcon, ShapeType.LINE);
        add(this.lineButton);

        Icon rectangleIcon = new ImageIcon(resizeIcon("../../images/rectangle.png"));
        this.rectangleButton = new ShapeButton(rectangleIcon, ShapeType.RECTANGLE);
        add(this.rectangleButton);

        Icon ellipseIcon = new ImageIcon(resizeIcon("../../images/ellipse.png"));
        this.ellipseButton = new ShapeButton(ellipseIcon, ShapeType.ELLIPSE);
        add(this.ellipseButton);

        Icon polygonIcon = new ImageIcon(resizeIcon("../../images/polygon.png"));
        this.polygonButton = new ShapeButton(polygonIcon, ShapeType.POLYGON);
        add(this.polygonButton);

        // Make all shape buttons observer
        shapeConfig.attachObserver(this.plotButton);
        shapeConfig.attachObserver(this.lineButton);
        shapeConfig.attachObserver(this.rectangleButton);
        shapeConfig.attachObserver(this.ellipseButton);
        shapeConfig.attachObserver(this.polygonButton);
    }

    /**
     * Resized the ImageIcon to fit the shapeButton.
     *
     * @param path path to image
     * @return resized image
     */
    private Image resizeIcon(String path) {
        ImageIcon icon = new ImageIcon(getClass().getResource(path));
        Image image = icon.getImage();
        return image.getScaledInstance(40, 40, Image.SCALE_SMOOTH);
    }

    /**
     * Adds actionListener that is passed in the parameter to all shape buttons.
     *
     * @param actionListener Shape Button Listener
     */
    public void addActionListener(ActionListener actionListener) {
        this.plotButton.addActionListener(actionListener);
        this.lineButton.addActionListener(actionListener);
        this.rectangleButton.addActionListener(actionListener);
        this.ellipseButton.addActionListener(actionListener);
        this.polygonButton.addActionListener(actionListener);
    }
}
