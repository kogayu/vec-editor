package veceditor.View;

import veceditor.MainFrame;
import veceditor.Model.DrawConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * JPanel that acts as a canvas for drawing.
 * Uses drawConfig to draw on itself.
 */
public class CanvasPanel extends JPanel {

    private DrawConfig config;

    public CanvasPanel(MainFrame frame, DrawConfig config) {
        this.config = config;

        setBackground(Color.WHITE);
        setPreferredSize(new Dimension(this.config.getCanvasLength(), this.config.getCanvasLength()));

        // Resize itself whenever the main window is resized.
        frame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                // if width > height
                if (frame.getWidth() > frame.getHeight()) {
                    // use height
                    config.setCanvasLength(frame.getHeight() * 8 / 10);
                } else {
                    // use width
                    config.setCanvasLength((frame.getWidth() - MainFrame.WIDTH / 5) * 8 / 10);
                }
                setPreferredSize(new Dimension(config.getCanvasLength(), config.getCanvasLength()));
                repaint();
            }
        });
    }

    /**
     * Call the drawing methods in DrawConfig and repaint the campus.
     *
     * @param g Graphics Component.
     */
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        this.config.drawShapes(g2, this.config.getCanvasLength());
        if (this.config.isDrawing()) {
            this.config.followCursorAndDraw(g);
        }
    }
}
