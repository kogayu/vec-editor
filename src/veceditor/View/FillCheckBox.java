package veceditor.View;

import veceditor.Model.FillColorConfig;
import veceditor.Observer.Observer;
import veceditor.Observer.Subject;

import javax.swing.*;

/**
 * Check box that enables / disables fill color.
 * Observes FillColorConfig.
 */
public class FillCheckBox extends JCheckBox implements Observer {

    public FillCheckBox(String text) {
        super(text);
    }

    /**
     * update state of checkbox depending on the state of FillColorConfig.
     *
     * @param s The subject that has been updated.
     */
    @Override
    public void update(Subject s) {
        FillColorConfig model = (FillColorConfig) s;
        setSelected(model.isEnabled());
    }
}
