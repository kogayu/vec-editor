package veceditor.View;

import veceditor.Model.FillColorConfig;
import veceditor.Model.PenColorConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * JPanel which holds color buttons
 * Add action listener to these color buttons
 */
public class ColorButtonsPanel extends JPanel {
    private JLabel penColorLabel;
    private JLabel fillColorLabel;
    private ColorButton fillColorButton;
    private ColorButton penColorButton;
    private FillCheckBox enableFill;

    public ColorButtonsPanel(FillColorConfig fillColorConfig, PenColorConfig penColorConfig) {
        super(new GridLayout(0, 2, 5, 5));

        setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10)));

        // attach fillColorButton (observer) to FillColorConfig (subject)
        this.fillColorButton = new ColorButton(fillColorConfig.getCurrentColor(), true);
        fillColorConfig.attachObserver(this.fillColorButton);

        // attach penColorButton (observer) to penColorConfig (subject)
        this.penColorButton = new ColorButton(penColorConfig.getCurrentColor(), false);
        penColorConfig.attachObserver(this.penColorButton);

        this.penColorLabel = new JLabel("Pen");
        this.fillColorLabel = new JLabel("Fill");

        // initialize, and attach Fill CheckBox to fillColorConfig
        this.enableFill = new FillCheckBox("fill");
        fillColorConfig.attachObserver(this.enableFill);

        // Keep all label center and push it to top for design purpose.
        penColorLabel.setHorizontalAlignment(JLabel.CENTER);
        fillColorLabel.setHorizontalAlignment(JLabel.CENTER);
        this.penColorLabel.setVerticalAlignment(JLabel.NORTH);
        this.fillColorLabel.setVerticalAlignment(JLabel.NORTH);

        add(this.fillColorButton);
        add(this.penColorButton);

        add(this.fillColorLabel);
        add(this.penColorLabel);

        add(this.enableFill);
    }

    /**
     * Return instance of FillColorButton
     *
     * @return instance of ColorButton
     */
    public ColorButton getFillColorButton() {
        return this.fillColorButton;
    }

    /**
     * Return instance of PenColorButton
     *
     * @return instance of ColorButton
     */
    public ColorButton getPenColorButton() {
        return this.penColorButton;
    }

    /**
     * Return instance of JCheckBox with label "fill"
     *
     * @return instance of JCheckBox
     */
    public JCheckBox getEnableFill() {
        return this.enableFill;
    }

    /**
     * Adds actionListener that is passed in the parameter to all ColorButton and JMenuItem.
     *
     * @param actionListener Color Button Listener
     */
    public void addActionListener(ActionListener actionListener) {
        this.fillColorButton.addActionListener(actionListener);
        this.penColorButton.addActionListener(actionListener);
        this.enableFill.addActionListener(actionListener);
    }
}


