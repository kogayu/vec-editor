package veceditor.View;

import veceditor.Controller.ColorController;
import veceditor.Controller.ShapeController;
import veceditor.Model.FillColorConfig;
import veceditor.Model.PenColorConfig;
import veceditor.Model.ShapeConfig;

import javax.swing.*;
import java.awt.*;

/**
 * Panel Class for the Side Menu.
 */
public class SidePanel extends JPanel {

    public SidePanel(int width, int height, ShapeConfig shapeConfig, FillColorConfig fillColorConfig, PenColorConfig penColorConfig) {
        setPreferredSize(new Dimension(width, height));
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createBevelBorder(0));

        ShapeButtonsPanel shapeButtonsPanel = new ShapeButtonsPanel(shapeConfig);
        add(shapeButtonsPanel, BorderLayout.NORTH);
        ShapeController controller = new ShapeController(shapeConfig, shapeButtonsPanel);

        ColorButtonsPanel colorButtonsPanel = new ColorButtonsPanel(fillColorConfig, penColorConfig);
        add(colorButtonsPanel, BorderLayout.SOUTH);
        ColorController colorButtonController = new ColorController(fillColorConfig, penColorConfig, colorButtonsPanel);
    }
}


