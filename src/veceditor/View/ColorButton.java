package veceditor.View;

import veceditor.Model.FillColorConfig;
import veceditor.Model.PenColorConfig;
import veceditor.Observer.Observer;
import veceditor.Observer.Subject;

import javax.swing.*;
import java.awt.*;

/**
 * Color button which will define current pen/fill Color.
 * Observes FillColorConfig and PenColorConfig
 */
public class ColorButton extends JButton implements Observer {
    private static int width = 50;
    private static int height = 50;
    private String buttonLabel = "X";

    public ColorButton(Color defaultColor, Boolean isFill) {
        // set initial color of button and background
        setPreferredSize(new Dimension(width, height));
        setBackground(defaultColor);
        if (isFill) {
            setText(buttonLabel);
            setBackground(null);
        }
    }

    public String getButtonLabel() {
        return this.buttonLabel;
    }

    @Override
    public void update(Subject s) {
        if (s instanceof FillColorConfig) {
            FillColorConfig model = (FillColorConfig) s;
            if (model.isEnabled()) {
                this.buttonLabel = null;
            } else {
                this.buttonLabel = "X";
            }
            setText(getButtonLabel());
            setBackground((model.isEnabled()) ? model.getCurrentColor() : null);

        } else if (s instanceof PenColorConfig) {
            PenColorConfig model = (PenColorConfig) s;
            setBackground(model.getCurrentColor());
        }

    }
}


