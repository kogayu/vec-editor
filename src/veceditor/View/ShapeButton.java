package veceditor.View;

import veceditor.Model.ShapeConfig;
import veceditor.Model.ShapeType;
import veceditor.Observer.Observer;
import veceditor.Observer.Subject;

import javax.swing.*;
import java.awt.*;

/**
 * Base class of all shape buttons.
 * Observes shapeConfig, and the selected Button's background
 * color will be set to LIGHT_GRAY.
 */
public class ShapeButton extends JButton implements Observer {
    private ShapeType shapeType;

    public ShapeButton(Icon icon, ShapeType shapeType) {
        super(icon);
        this.shapeType = shapeType;
        setPreferredSize(new Dimension(50, 70));
        setFocusPainted(false);
    }

    /**
     * Set background color to LIGHT_GRAY if the selected shapeType matches
     * the shapeType of the button.
     *
     * @param s The subject that has been updated.
     */
    @Override
    public void update(Subject s) {
        ShapeConfig model = (ShapeConfig) s;

        // Change background color if selected shape is this shape type
        if (this.shapeType == model.getCurrentShape()) {
            setBackground(Color.LIGHT_GRAY);
        } else {
            setBackground(null);
        }
    }
}
