package veceditor;

import veceditor.Controller.DrawController;
import veceditor.Controller.TopMenuBarController;
import veceditor.Model.DrawConfig;
import veceditor.Model.FillColorConfig;
import veceditor.Model.PenColorConfig;
import veceditor.Model.ShapeConfig;
import veceditor.View.CanvasPanel;
import veceditor.View.SidePanel;
import veceditor.View.TopMenuBar;

import javax.swing.*;
import java.awt.*;

/**
 * Base frame of the application
 */
public class MainFrame extends JFrame {
    /**
     * Initial width of the window
     */
    public static final int WIDTH = 1000;

    /**
     * Initial height of the window
     */
    public static final int HEIGHT = 700;

    /**
     * Initialize all components of the application
     */
    public MainFrame() {
        super("Vec Editor");
        // Set cross-platform look and feel
        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        // Initialize window in center of the screen
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dimension.width / 2 - getSize().width / 2, dimension.height / 2 - getSize().height / 2);

        // Initialize shape, fillColor, penColor and draw config
        ShapeConfig shapeConfig = new ShapeConfig();
        FillColorConfig fillColorConfig = new FillColorConfig();
        PenColorConfig penColorConfig = new PenColorConfig();
        DrawConfig drawConfig = new DrawConfig(shapeConfig, penColorConfig, fillColorConfig, HEIGHT * 8 / 10);

        // Initialize and add side panel
        SidePanel sidePanel = new SidePanel(WIDTH / 5, HEIGHT, shapeConfig, fillColorConfig, penColorConfig);

        add(sidePanel, BorderLayout.WEST);

        // Initialize and add canvas panel
        CanvasPanel canvasPanel = new CanvasPanel(this, drawConfig);
        JPanel contentPanel = new JPanel();
        contentPanel.setLayout(new GridBagLayout());
        contentPanel.add(canvasPanel);
        add(contentPanel, BorderLayout.CENTER);

        // Initialize draw controller
        DrawController drawController = new DrawController(drawConfig, canvasPanel);

        // Initialize and add top menu bar
        TopMenuBar topMenuBar = new TopMenuBar();
        add(topMenuBar, BorderLayout.NORTH);

        // attach DrawConfig (observer) to shapeConfig, penColorConfig and fillColorConfig (subject)
        shapeConfig.attachObserver(drawConfig);
        penColorConfig.attachObserver(drawConfig);
        fillColorConfig.attachObserver(drawConfig);

        TopMenuBarController topMenuBarController = new TopMenuBarController(canvasPanel,
                drawConfig, topMenuBar);
        setVisible(true);
    }
}
