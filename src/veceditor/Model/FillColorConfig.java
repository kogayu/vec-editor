package veceditor.Model;

/**
 * This class extends ColorConfig class. It will store fill color.
 */
public class FillColorConfig extends ColorConfig {
    public FillColorConfig() {
        // is disabled by default
        super(false);
    }

}
