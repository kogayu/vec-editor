package veceditor.Model;

import veceditor.Observer.Subject;

import java.awt.*;

/**
 * Abstract class for all other color configuration class.
 * It has methods for all color operations.
 */
public abstract class ColorConfig extends Subject {
    private Color currentColor;
    private boolean enabled; // for fill color config only

    public ColorConfig(Boolean enabled) {
        this.currentColor = Color.BLACK;
        this.enabled = enabled;
    }

    /**
     * Get currently set color
     *
     * @return currently set color
     */
    public Color getCurrentColor() {
        return this.currentColor;
    }

    /**
     * Set new color
     *
     * @param newColor instance of Color class
     */
    public void setCurrentColor(Color newColor) {
        this.currentColor = newColor;
        notifyObservers();
    }

    /**
     * Check whether fill is enabled
     *
     * @return If fill is enabled, it returns true, otherwise false.
     */
    public boolean isEnabled() {
        return this.enabled;
    }

    /**
     * Update fill color status
     *
     * @param enabled true will enable fill color, false will not.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        notifyObservers();
    }
}
