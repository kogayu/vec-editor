package veceditor.Model;

import veceditor.Exception.InvalidColorException;
import veceditor.Exception.InvalidCommandException;
import veceditor.Exception.InvalidImageSizeException;
import veceditor.View.CanvasPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class holds and tracks all configurations for file,
 * and has methods for all file operations.
 */
public class FileConfig {

    private File file;
    private JFileChooser fcForVec;
    private JFileChooser fcForBmp;
    private DrawConfig drawConfig;

    /**
     * set boolean value of testing
     *
     * @param testing true if it is testing
     */
    public void setTesting(Boolean testing) {
        this.testing = testing;
    }

    /**
     * Testing flag which is used to determine whether it is in a test or not.
     * Only set to false when FileConfig is initialized in a test.
     */
    private Boolean testing;

    public FileConfig(DrawConfig drawConfig) {
        this.drawConfig = drawConfig;
        FileNameExtensionFilter fcForVecFilter = new FileNameExtensionFilter("VEC file", "vec"); // Only allow vec extension
        FileNameExtensionFilter fcForBmpFilter = new FileNameExtensionFilter("BMP file", "bmp"); // Only allow vec extension

        // File chooser tailored for vec file
        this.fcForVec = new JFileChooser();
        this.fcForVec.setFileSelectionMode(JFileChooser.FILES_ONLY);
        this.fcForVec.setFileFilter(fcForVecFilter);
        this.fcForVec.setAcceptAllFileFilterUsed(false);

        // File chooser tailored for bmp file
        this.fcForBmp = new JFileChooser();
        this.fcForBmp.setFileSelectionMode(JFileChooser.FILES_ONLY);
        this.fcForBmp.setFileFilter(fcForBmpFilter);
        this.fcForBmp.setAcceptAllFileFilterUsed(false);

        this.testing = false;
    }

    /**
     * Set new file
     *
     * @param file　new file instance
     */
    public void setFile(File file) {
        this.file = file;
    }

    /**
     * Return currently set file
     *
     * @return currently set file
     */
    public File getFile() {
        return this.file;
    }

    /**
     * Return FileChooser for vec file
     *
     * @return FileChooser for vec file
     */
    public JFileChooser getFcForVec() {
        return this.fcForVec;
    }

    /**
     * Return FileChooser for bmp file
     *
     * @return FileChooser for bmp file
     */
    public JFileChooser getFcForBmp() {
        return this.fcForBmp;
    }

    /**
     * Allow user to choose vec file and extract commands, and set on drawConfig
     *
     * @param source JMenuItem in TopMenuBar
     * @throws InvalidCommandException if the command is undefined or the number of vertices is invalid
     * @throws InvalidColorException if the color code in the command is invalid
     * @throws IOException if the file cannot be opened
     */
    public void drawCommandsFromFile(JMenuItem source) throws InvalidCommandException, IOException, InvalidColorException {
        int returnVal;
        returnVal = this.fcForVec.showOpenDialog(source);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            this.file = this.fcForVec.getSelectedFile();
            this.drawConfig.setCommands(translateFileToCommands(this.file)); // set commands of shapes to drawConfig, so that canvas can draw
        }
    }

    /**
     * (if user have not selected file) allow user to choose file and save commands on it
     *
     * @param source JMenuItem in TopMenuBar
     * @throws IOException if the file cannot be opened for some reason
     */
    public void saveOnFile(JMenuItem source) throws IOException {
        int returnVal;
        if (this.file == null) { // If user is not working on any file, create new file or choose existing file to save.
            returnVal = this.fcForVec.showSaveDialog(source); // Open dialog to choose vec file or create new file
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File selectedFile = this.fcForVec.getSelectedFile();
                if (selectedFile == null) {
                    return;
                }
                if (!selectedFile.getName().toLowerCase().endsWith(".vec")) {
                    selectedFile = new File(selectedFile.getParentFile(), selectedFile.getName() + ".vec"); // Create new file with .vec extension in file name
                    this.file = selectedFile; // Set this file as current selected file
                }
                try {
                    writeCommandsOnFile(this.drawConfig.getCommands(), selectedFile); // Write down current canvas information of the file and save
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else { // If user currently selects file, save updated canvas information on it.
            writeCommandsOnFile(this.drawConfig.getCommands(), this.file);
        }
    }

    /**
     * Allow user to set size of image, choose file to export and write commands on it.
     *
     * @param source      Instance of JMenuItem
     * @param canvasPanel Instance of CanvasPanel
     * @throws InvalidImageSizeException if the image size is invalid
     */
    public void exportToBmp(JMenuItem source, CanvasPanel canvasPanel) throws InvalidImageSizeException {
        int returnVal;
        // 1. Ask user the size of image they would like to generate
        // 2. Generate BufferedImage by commands and size
        BufferedImage bi = generateBmpFormatImage(askImageSize(canvasPanel));
        if (bi != null) { // Check generated BufferedImage is legit
            returnVal = this.fcForBmp.showSaveDialog(source);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File selectedFile = this.fcForBmp.getSelectedFile(); // Ask user file to save/generate new file
                if (selectedFile == null) { // If user cancels selecting file
                    return;
                }
                if (!selectedFile.getName().toLowerCase().endsWith(".bmp")) { // Make sure chosen file is bmp file
                    selectedFile = new File(selectedFile.getParentFile(), selectedFile.getName() + ".bmp"); // Add .bmp to name of the selected file
                }
                try {
                    RenderedImage rendImage = bi;
                    Boolean b = ImageIO.write(rendImage, "BMP", selectedFile); // Write image data on the selected file
                    if (b && !this.testing) {
                        JOptionPane.showMessageDialog(null, "File was successfully exported as bmp", "", JOptionPane.PLAIN_MESSAGE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Convert vec commands to an ArrayList of DrawCommands
     *
     * @param loadedFile get a vec file opened by user
     * @return ArrayList of DrawCommand which will be drawn on a canvas
     * @throws InvalidCommandException if the command is undefined or the number of vertices is invalid
     * @throws InvalidColorException if the color code in the command is invalid
     * @throws IOException if the file cannot be opened
     */
    public ArrayList<DrawCommand> translateFileToCommands(File loadedFile) throws InvalidCommandException, InvalidColorException, IOException {
        final String patternToFindType = "^[A-Z]+"; // Regex to find out ShapeType or Pen/Fill
        final String patternToFindVertices = "\\d+[\\.]\\d+"; // Regex to find out vertices

        String filename = loadedFile.getAbsolutePath();
        ArrayList<DrawCommand> commands = new ArrayList<DrawCommand>();
        String stringLine;

        Color penColor = Color.BLACK;
        Color fillColor = null;
        boolean fillEnabled = false;

        int currentIdx; // Count loop for assigning vertice to specific index in array
        Float[] verticesInArray; // Store vertices if ShapeType is not POLYGON
        ArrayList<Float> verticesInArrayList; // Store vertices is ShapeType is POLYGON

        // Read file...
        FileReader reader = new FileReader(filename);
        BufferedReader br = new BufferedReader(reader);

        // Read line by line
        while ((stringLine = br.readLine()) != null) {
            DrawCommand dc; // Create new DrawCommand instance
            ShapeType shapeType = null;
            Pattern p = Pattern.compile(patternToFindType); // Set regex for finding shape type or pen type
            Matcher m = p.matcher(stringLine);
            if (m.find()) {
                String matched = m.group();  // If matched string is ShapeType, assign ShapeType and go finding coordinates. If its pen/fill color, set new color and go to next iteration.
                switch (matched) {
                    case "PLOT":
                        shapeType = ShapeType.PLOT;
                        break;
                    case "LINE":
                        shapeType = ShapeType.LINE;
                        break;
                    case "RECTANGLE":
                        shapeType = ShapeType.RECTANGLE;
                        break;
                    case "ELLIPSE":
                        shapeType = ShapeType.ELLIPSE;
                        break;
                    case "POLYGON":
                        shapeType = ShapeType.POLYGON;
                        break;
                    case "PEN":
                        penColor = hexToColor(stringLine);
                        continue;
                    case "FILL":
                        Boolean isFillOff = checkFillOff(stringLine);
                        if (isFillOff) {
                            fillEnabled = false;
                        } else {
                            if (!fillEnabled) fillEnabled = true;
                            fillColor = hexToColor(stringLine);
                        }
                        continue;
                    default:
                        throw new InvalidCommandException(String.format("Unknown Command \"%s\" in file", matched), this.testing);
                }
            }

            // If matched string is ShapeType, extract coordinates
            p = Pattern.compile(patternToFindVertices);
            m = p.matcher(stringLine);
            verticesInArrayList = new ArrayList<>();

            while (m.find()) {
                float vertex = Float.parseFloat(m.group());
                if (vertex <= 1.0) {
                    verticesInArrayList.add(vertex);
                } else {
                    throw new InvalidCommandException(String.format("Invalid value of vertices (%f) in command: \"%s\"", vertex, stringLine), this.testing);
                }
            }

            if (shapeType == ShapeType.PLOT && verticesInArrayList.size() == 2) {
                // In vec file, PLOT will just contain only one x and y coordinate, therefore
                // duplicate each x and y and store in array.
                verticesInArrayList.add(verticesInArrayList.get(0));
                verticesInArrayList.add(verticesInArrayList.get(1));
            }

            // validate size of the array of vertices
            if (shapeType != ShapeType.POLYGON && verticesInArrayList.size() != 4) {
                throw new InvalidCommandException(String.format("Invalid number of vertices (%d) in command: \"%s\"", verticesInArrayList.size(), stringLine), this.testing);
            }

            // Convert ArrayList to Float Array
            verticesInArray = verticesInArrayList.toArray(new Float[verticesInArrayList.size()]);

            dc = new DrawCommand(shapeType, penColor, fillColor, fillEnabled, verticesInArray);
            commands.add(dc);
        }
        return commands;
    }

    /**
     * @param command string of FILL command from file
     * @return true if command includes OFF, false if first character is #
     * @throws InvalidColorException If command is neither OFF or starting with #, this exception will be thrown
     */
    public Boolean checkFillOff(String command) throws InvalidColorException {
        String[] commands = command.split(" ");
        if (commands[1].charAt(0) == '#') {
            return false;
        } else if (commands[1].equals("OFF")) {
            return true;
        } else {
            throw new InvalidColorException(String.format("Invalid FILL syntax"), this.testing);
        }
    }

    /**
     * Convert hex color to Color class
     *
     * @param stringLine String of a line from vec file
     * @return new Color object which has color from stringLine
     * @throws InvalidColorException if the color code in the command is invalid
     */
    public Color hexToColor(String stringLine) throws InvalidColorException {
        final String patternToFindHexColor = "#[\\d|A-Z]+"; // Regex to find out color in hex
        Pattern p = Pattern.compile(patternToFindHexColor); // Set regex to find out hex color.
        Matcher m = p.matcher(stringLine);
        if (m.find()) {
            try {
                String colorInHex = m.group(0);
                return new Color(
                        Integer.valueOf(colorInHex.substring(1, 3), 16),
                        Integer.valueOf(colorInHex.substring(3, 5), 16),
                        Integer.valueOf(colorInHex.substring(5, 7), 16));
            } catch (NumberFormatException | StringIndexOutOfBoundsException e) {
                e.printStackTrace();
                throw new InvalidColorException(String.format("Invalid color code in file: \"%s\"", m.group(0)), this.testing);
            }
        } else {
            throw new InvalidColorException(String.format("Color was not found in command: \"%s\"", stringLine), this.testing);
        }
    }

    /**
     * Write commands of shapes and colors on selected file, and save it.
     *
     * @param commands     ArrayList of commands. They are information of all shapes onFile selectedFile canvas
     * @param selectedFile Instance of File class. It is selected file by user. All commands will be written on this.
     * @throws IOException if the file cannot be opened
     */
    public void writeCommandsOnFile(ArrayList<DrawCommand> commands, File selectedFile) throws IOException {
        Color penColor = Color.BLACK;
        Color fillColor = null;
        BufferedWriter writer;

        writer = new BufferedWriter(new FileWriter(selectedFile));
        Boolean isFillEnabled = false;  // This variable will be used to assign "FILL OFF" command in file
        Iterator i = commands.iterator();
        while (i.hasNext()) {
            DrawCommand command = (DrawCommand) i.next();
            if (!command.isFillEnabled() && isFillEnabled) { // If previous shape turns on fill color, and current shape doesn't, assign "FILL OFF" command
                writer.append("FILL OFF\n");
                isFillEnabled = false;
            }
            if (command.getPenColor() != penColor) { // If pen color of shape is different from previous shapes, assign "PEN #HEXNUM" command to file
                penColor = command.getPenColor();
                writer.append(String.format("PEN %s\n", this.colorToHex(command.getPenColor())));
            }
            // if fill color in command is different to current configured fill color
            if (command.getFillColor() != fillColor) {
                fillColor = command.getFillColor();
            }
            // only if fill is enabled, add FILL command
            if (command.isFillEnabled()) { // If current shape accepts fill color, assign "FILL #HEXNUM" command
                isFillEnabled = true;
                writer.append(String.format("FILL %s\n", this.colorToHex(command.getFillColor())));
            }
            writer.append(String.format("%s %s\n", command.getShape(), verticesToString(command.getVertices(), command.getShape() == ShapeType.PLOT)));
        }
        writer.close(); // Save changes on file
        if (!this.testing) {
            JOptionPane.showMessageDialog(null, "File saved.", "", JOptionPane.PLAIN_MESSAGE);
        }
    }

    /**
     * Convert color in hex number
     *
     * @param color instance of Color class
     * @return String of hex number
     */
    private String colorToHex(Color color) {
        return String.format("#%02X%02X%02X", color.getRed(), color.getGreen(), color.getBlue());
    }

    /**
     * Convert vertices in float to string
     *
     * @param vertices Array of float
     * @return String of float numbers
     */
    private String verticesToString(Float[] vertices, boolean plot) {
        String s = "";
        // if shape type is plot, only use the first two x and y for the sake of VEC.
        if (plot) {
            vertices = new Float[]{vertices[0], vertices[1]};
        }
        for (int i = 0; i < vertices.length; i++) {
            if (i == vertices.length - 1) {
                s = s.concat(String.format("%06f", vertices[i], 6)); // round to 6 decimal places, otherwise fill with 0
            } else {
                s = s.concat(String.format("%06f ", vertices[i], 6)); // round to 6 decimal places, otherwise fill with 0
            }
        }
        return s;
    }

    /**
     * Loop through ArrayList of DrawCommand to generate BufferedImage
     *
     * @param size Size of image user indicated
     * @return BufferedImage generated with commands
     */

    private BufferedImage generateBmpFormatImage(Integer size) {
        if (size == null) return null; // If user cancels to choose size
        BufferedImage bufferedImage;
        Graphics2D graphics2D;

        bufferedImage = new BufferedImage(size, size, BufferedImage.TYPE_INT_RGB);
        graphics2D = bufferedImage.createGraphics();
        graphics2D.setPaint(Color.WHITE);
        graphics2D.fillRect(0, 0, size, size);
        this.drawConfig.drawShapes(graphics2D, size);

        graphics2D.dispose();
        return bufferedImage;
    }

    /**
     * Ask user the size of image to generate
     *
     * @return Size of image
     */
    private Integer askImageSize(CanvasPanel canvasPanel) throws InvalidImageSizeException {
        Integer size = null;
        while (size == null) {

            String s = JOptionPane.showInputDialog(
                    canvasPanel,
                    "Please select bitmap dimension in pixel (Maximum: 4096)",
                    "Define bitmap size",
                    JOptionPane.PLAIN_MESSAGE);
            if (s == null) {
                return null;
            }
            size = convertStringToInt(s);
            if (!checkImageSize(size)) {
                size = null;
            }
        }
        return size;
    }

    /**
     * Check whether user specifies image size by non number.
     *
     * @param size of image user provided
     * @return if user input is number, return integer parsed from
     * @throws InvalidImageSizeException Exception will be thrown if user input for image size is not integer.
     */
    public int convertStringToInt(String size) throws InvalidImageSizeException {
        try {
            return (Integer.valueOf(size));
        } catch (NumberFormatException e) {
            throw new InvalidImageSizeException(String.format("Invalid size: \"%s\"", size), this.testing);
        }
    }

    /**
     * Check image size user specified is valid
     *
     * @param size size of image in int
     * @return if image size is between 0 ~ 4096, return true, otherwise throw exception
     * @throws InvalidImageSizeException Exception will be thrown if size is bigger than 4096 or smaller than or equals to 0
     */
    public boolean checkImageSize(int size) throws InvalidImageSizeException {
        if (size > 4096 || size <= 0) {
            throw new InvalidImageSizeException(String.format("Invalid size: \"%d\"", size), this.testing);
        } else {
            return true;
        }
    }

}
