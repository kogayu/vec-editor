package veceditor.Model;

import veceditor.Observer.Subject;

/**
 * Holds current config for drawing.
 */
public class ShapeConfig extends Subject {
    private ShapeType currentShape;

    public ShapeConfig() {
        // initialize shape with plot
        this.currentShape = ShapeType.PLOT;
    }

    /**
     * Gets current shape type.
     *
     * @return current shape type
     */
    public ShapeType getCurrentShape() {
        return currentShape;
    }

    /**
     * Sets new shape type.
     *
     * @param shapeType type of shape.
     */
    public void setCurrentShape(ShapeType shapeType) {
        this.currentShape = shapeType;
        System.out.println("notifying observers with new shape type: " + shapeType);
        notifyObservers();
    }

}
