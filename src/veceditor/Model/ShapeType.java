package veceditor.Model;

public enum ShapeType {
    PLOT,
    LINE,
    RECTANGLE,
    ELLIPSE,
    POLYGON
}
