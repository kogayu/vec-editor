package veceditor.Model;

/**
 * This class extends ColorConfig class. It will store pen color.
 */
public class PenColorConfig extends ColorConfig {
    public PenColorConfig() {
        // is always enabled
        super(true);
    }
}
