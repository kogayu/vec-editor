package veceditor.Model;

import veceditor.Observer.Observer;
import veceditor.Observer.Subject;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.geom.GeneralPath;
import java.util.ArrayList;

/**
 * This class holds and tracks all configurations for drawing,
 * and has methods for all drawing operations.
 * Observes shapeConfig, fillColorConfig and penColorConfig.
 */
public class DrawConfig implements Observer {
    private ShapeType shapeType; // current shape type. updated by ShapeConfig.
    private Color penColor; // current pen color. updated by PenColorConfig.
    private Color fillColor; // current fill color. updated by FillColorConfig.
    private boolean fillIsEnabled; // current state of fill (true: enabled, false: disabled)
    private ArrayList<Float> coordinates = new ArrayList<>(); // list to hold coordinates for each shape that's drawn.
    private int polyVertices = 0; // integer to keep track of the number of vertices of the polygon that's being drawn.
    private int canvasLength; // holds current length of the canvas panel

    private ArrayList<DrawCommand> commands = new ArrayList<>(); // list of drawing commands.
    private boolean drawing = false; // flag to identify if canvas is in a drawing state or not.

    public DrawConfig(ShapeConfig shapeConfig, PenColorConfig penConfig, FillColorConfig fillConfig, int edgeLength) {
        // set initial drawing configurations
        this.shapeType = shapeConfig.getCurrentShape();
        this.penColor = penConfig.getCurrentColor();
        this.fillColor = fillConfig.getCurrentColor();
        this.canvasLength = edgeLength;
    }

    /**
     * Updates shapeType, penColor, fillColor and fillIsEnabled depending on
     * what subject calls this method.
     *
     * @param s The subject that has been updated.
     */
    @Override
    public void update(Subject s) {
        if (s instanceof ShapeConfig) {
            ShapeConfig model = (ShapeConfig) s;
            this.shapeType = model.getCurrentShape();
            System.out.println("Observer notified. New Shape Type: " + this.shapeType);
        } else if (s instanceof PenColorConfig) {
            PenColorConfig model = (PenColorConfig) s;
            this.penColor = model.getCurrentColor();
            System.out.println("Observer notified. New Pen Color: " + this.penColor);
        } else if (s instanceof FillColorConfig) {
            FillColorConfig model = (FillColorConfig) s;
            this.fillColor = model.getCurrentColor();
            this.fillIsEnabled = model.isEnabled();
            System.out.println("Observer notified. New Fill Color: " + this.fillColor);
        }
    }

    /**
     * Checks if current state of drawing is true or not.
     *
     * @return boolean value of drawing flag.
     */
    public boolean isDrawing() {
        return this.drawing;
    }

    /**
     * Gets current length of the canvas panel.
     *
     * @return length of canvas (int)
     */
    public int getCanvasLength() {
        return this.canvasLength;
    }

    /**
     * Sets length of canvas to the provided value.
     *
     * @param length: new length of canvas.
     */
    public void setCanvasLength(int length) {
        this.canvasLength = length;
    }

    /**
     * Gets the list of coordinates. Used only in unit tests.
     *
     * @return List of coordinates.
     */
    public ArrayList<Float> getCoordinates() {
        return this.coordinates;
    }

    /**
     * Get the list of drawCommands.
     *
     * @return List of drawCommands.
     */
    public ArrayList<DrawCommand> getCommands() {
        return this.commands;
    }

    /**
     * Set commands from loaded VEC file
     *
     * @param loadedCommands: List of commands
     */
    public void setCommands(ArrayList<DrawCommand> loadedCommands) {
        this.commands = loadedCommands;
    }

    /**
     * Adds drawing command to the list of commands.
     */
    public void addCommand() {
        // set coordinate to 1 if it is greater than 1.
        for (Float coord : this.coordinates) {
            if (coord > 1f) {
                this.coordinates.set(this.coordinates.indexOf(coord), 1f);
            }
        }
        // create Float array of coordinates to pass it to DrawCommand.
        Float[] arrayCoords = new Float[this.coordinates.size()];
        arrayCoords = this.coordinates.toArray(arrayCoords);

        // create new command with the current drawing configurations and the array of coordinates.
        DrawCommand newCommand = new DrawCommand(this.shapeType, this.penColor, this.fillColor, this.fillIsEnabled, arrayCoords);
        // add the command to the list of commands.
        this.commands.add(newCommand);

        // clear coordinates.
        this.coordinates.clear();
        this.polyVertices = 0;
        for (DrawCommand command : this.commands) {
            System.out.println(command.toString());
        }
    }

    /**
     * Loops through the list of commands, and call appropriate drawing functions from Graphics2D.
     *
     * @param g2:   Graphics instance passed from paintComponent.
     * @param size: Canvas size in integer
     */
    public void drawShapes(Graphics2D g2, int size) {
        for (DrawCommand command : this.commands) {
            // set pen color first
            g2.setPaint(command.getPenColor());

            // if shape is not a POLYGON
            if (command.getShape() != ShapeType.POLYGON) {
                int x1 = vecToCoords(command.getVertices()[0], size);
                int y1 = vecToCoords(command.getVertices()[1], size);
                int x2 = vecToCoords(command.getVertices()[2], size);
                int y2 = vecToCoords(command.getVertices()[3], size);

                if (command.getShape() == ShapeType.PLOT || command.getShape() == ShapeType.LINE) {
                    g2.drawLine(x1, y1, x2, y2);
                } else if (command.getShape() == ShapeType.RECTANGLE) {
                    g2.drawRect(x1, y1, x2 - x1, y2 - y1);
                    if (command.isFillEnabled()) {
                        g2.setPaint(command.getFillColor());
                        g2.fillRect(x1, y1, x2 - x1, y2 - y1);
                    }
                } else if (command.getShape() == ShapeType.ELLIPSE) {
                    g2.drawOval(x1, y1, x2 - x1, y2 - y1);
                    if (command.isFillEnabled()) {
                        g2.setPaint(command.getFillColor());
                        g2.fillOval(x1, y1, x2 - x1, y2 - y1);
                    }
                }
            } else {
                // if shape is a polygon
                GeneralPath polygon = new GeneralPath();
                // move path to the starting coordinates
                polygon.moveTo(vecToCoords(command.getVertices()[0], size), vecToCoords(command.getVertices()[1], size));
                // for each coordinate, draw a line from the previous coordinates.
                for (int i = 2; i < command.getVertices().length; i += 2) {
                    polygon.lineTo(vecToCoords(command.getVertices()[i], size), vecToCoords(command.getVertices()[i + 1], size));
                }
                // close the polygon (connect the last coordinates with the starting coordinates)
                // once all lines are drawn.
                polygon.closePath();
                // draw the polygon
                g2.draw(polygon);
                if (command.isFillEnabled()) {
                    g2.setPaint(command.getFillColor());
                    g2.fill(polygon);
                }
            }
        }
    }

    /**
     * Method that renders the shape when it is being drawn.
     * This makes it possible to follow the cursor to adjust the size
     * or vertices of the shape.
     *
     * @param g: Graphics instance passed from paintComponent.
     */
    public void followCursorAndDraw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        // set pen color first
        g2.setPaint(this.penColor);
        if (this.coordinates.size() == 4 && this.shapeType != ShapeType.POLYGON) {
            int x1 = vecToCoords(this.coordinates.get(0), this.canvasLength);
            int y1 = vecToCoords(this.coordinates.get(1), this.canvasLength);
            int x2 = vecToCoords(this.coordinates.get(2), this.canvasLength);
            int y2 = vecToCoords(this.coordinates.get(3), this.canvasLength);

            if (this.shapeType == ShapeType.LINE) {
                g2.drawLine(x1, y1, x2, y2);
            } else if (this.shapeType == ShapeType.RECTANGLE) {
                g2.drawRect(x1, y1, x2 - x1, y2 - y1);
            } else if (this.shapeType == ShapeType.ELLIPSE) {
                g2.drawOval(x1, y1, x2 - x1, y2 - y1);
            }
        }

        if (this.shapeType == ShapeType.POLYGON) {
            // pretty much the same process as in drawShapes, but this one doesn't close the polygon.
            GeneralPath polygon = new GeneralPath();
            polygon.moveTo(vecToCoords(this.coordinates.get(0), this.canvasLength), vecToCoords(this.coordinates.get(1), this.canvasLength));
            for (int i = 2; i < this.coordinates.size(); i += 2) {
                polygon.lineTo(vecToCoords(this.coordinates.get(i), this.canvasLength), vecToCoords(this.coordinates.get(i + 1), this.canvasLength));
            }
            g2.draw(polygon);
        }
    }

    /**
     * Remove last command in the commands list to undo latest operation.
     */
    public void undo() {
        if (this.commands.size() > 0) {
            this.commands.remove(this.commands.size() - 1);
        }
    }

    /**
     * Convert coordinate relative to CanvasPanel to Vec format (0.0 - 1.0)
     *
     * @param coordinate: either X or Y relative to CanvasPanel
     * @return coordinate divided by the current length of the edge of the CanvasPanel.
     */
    private float coordsToVec(int coordinate) {
        return (float) coordinate / this.canvasLength;
    }

    /**
     * Convert vec format coordinate to coordinates relative to CanvalPanel.
     *
     * @param vec:    either X or Y in vec format (0.0 - 1.0)
     * @param length: length of canvas
     * @return vec multiplied by current length of the edge of the CanvasPanel.
     */
    private int vecToCoords(float vec, int length) {
        return Math.round(vec * length);
    }

    /**
     * Called when mouse if pressed on the CanvasPanel.
     * Sets this.drawing to true and adds the starting coordinates for
     * shapes other than PLOT.
     * For PLOT, this adds the current coordinates twice and set drawing to false,
     * and then call addCommand() to end the drawing.
     *
     * @param x: x coordinate of MousePress event.
     * @param y: y coordinate of MousePress event.
     */
    public void processMousePress(int x, int y) {
        // don't do anything if shape type is PLOT or POLYGON.
        if (this.shapeType != ShapeType.PLOT) {
            // set drawing flag to true
            this.drawing = true;

            // add x and y of the mouse event to coordinates (starting point)
            this.coordinates.add(coordsToVec(x));
            this.coordinates.add(coordsToVec(y));

            if (this.shapeType == ShapeType.POLYGON) {
                this.polyVertices++; // increment the number of vertices for every mouse press.
            }
        } else {
            // if shapeType is PLOT
            // set the same start and end coordinates
            this.coordinates.add(coordsToVec(x));
            this.coordinates.add(coordsToVec(y));
            this.coordinates.add(coordsToVec(x));
            this.coordinates.add(coordsToVec(y));

            this.drawing = false;
            addCommand();
        }
    }

    /**
     * Called when mouse is released on the CanvasPanel.
     * For shapes other than PLOT and POLYGON, this ends the drawing
     * by setting this.drawing to false, and updating the ending coordinates.
     * Then calls addCommand.
     *
     * @param x: x coordinate of MouseRelease event.
     * @param y: y coordinate of MouseRelease event.
     */
    public void processMouseRelease(int x, int y) {
        // don't do anything if shape type is PLOT or POLYGON.
        if (this.shapeType != ShapeType.PLOT && this.shapeType != ShapeType.POLYGON) {
            // set drawing to false
            this.drawing = false;

            // if coordinates have both starting and ending points
            if (this.coordinates.size() == 4) {
                // set final ending points to current X and Y.
                this.coordinates.set(2, coordsToVec(x));
                this.coordinates.set(3, coordsToVec(y));

                // Add shape to command.
                addCommand();
            }
        }
    }

    /**
     * Called when mouse is dragged on the CanvasPanel.
     * If the shape is neither PLOT nor POLYGON, it either:
     * - updates the ending coordinates if there already exists the ending coordinates.
     * - adds the ending coordinates to the list if there aren't any.
     *
     * @param x: x coordinate of MouseDrag event.
     * @param y: y coordinate of MouseDrag event.
     */
    public void processMouseDrag(int x, int y) {
        if (this.shapeType != ShapeType.PLOT && this.shapeType != ShapeType.POLYGON) {
            // if coodinates have both starting and ending points
            if (this.coordinates.size() == 4) {
                // replace ending points with current X and Y
                this.coordinates.set(2, coordsToVec(x));
                this.coordinates.set(3, coordsToVec(y));
            } else {
                // otherwise, just add current X and Y as ending points.
                this.coordinates.add(coordsToVec(x));
                this.coordinates.add(coordsToVec(y));
            }
        }
    }

    /**
     * Called when mouse is moved on the CanvasPanel.
     * If this.drawing is true and this.shapeType is POLYGON,
     * it either:
     * - add the ending coordinates if the size of the coordinates list is
     * equal to the current number of vertices * 2.
     * - Otherwise, updates the last set of coordinates in the list.
     *
     * @param x: x coordinate of MouseMove event.
     * @param y: y coordinate of MouseMove event.
     */
    public void processMouseMove(int x, int y) {
        if (this.drawing && this.shapeType == ShapeType.POLYGON) {
            // if the number of vertices * 2 and the size of ArrayList coordinates match.
            if (this.coordinates.size() == this.polyVertices * 2) {
                // add current coordinates
                this.coordinates.add(coordsToVec(x));
                this.coordinates.add(coordsToVec(y));
            } else {
                // otherwise, replace the last two coordinates with the current.
                this.coordinates.set(this.coordinates.size() - 2, coordsToVec(x));
                this.coordinates.set(this.coordinates.size() - 1, coordsToVec(y));
            }
        }
    }

    /**
     * Called when some key is pressed.
     * If the pressed key is Cmd + Z or Ctrl + Z, it calles the undo method.
     * If the pressed key is the space bar and if the current shapeType is POLYGON
     * and this.drawing is true, it sets this.drawing to false and removes the
     * last sets of coordinates (which should be the current coordinates of the mouse).
     * Then calls addCommand().
     *
     * @param keyCode: key code (int)
     */
    public void processKeyPress(int keyCode) {
        if ((keyCode == KeyEvent.VK_Z)) {
            // undo functionality
            undo();
        } else if (keyCode == KeyEvent.VK_SPACE) {
            // finish drawing polygon once space key is pressed.
            if (this.shapeType == ShapeType.POLYGON && this.drawing) {
                this.drawing = false;
                // remove the last coordinates as it is just for following the mouse cursor.
                if (this.coordinates.size() >= 4) {
                    this.coordinates.remove(this.coordinates.size() - 1);
                    this.coordinates.remove(this.coordinates.size() - 1);
                }
                addCommand();
            }
        }
    }

}
