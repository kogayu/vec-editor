package veceditor.Model;

import java.awt.*;
import java.util.Arrays;

/**
 * Class to be instantiated for each shape that will be drawn.
 * Will be used in CanvasPanel as an ArrayList of drawing commands.
 */
public class DrawCommand {

    private ShapeType shape;
    private Color penColor;
    private Color fillColor;
    private boolean fillEnabled;
    private Float[] vertices;

    public DrawCommand(ShapeType shape, Color penColor, Color fillColor, Boolean fill, Float[] vertices) {
        this.shape = shape;
        this.penColor = penColor;
        this.fillColor = fillColor;
        this.vertices = vertices;
        this.fillEnabled = fill;
    }

    /**
     * Converts a drawCommand to string. Mainly for debugging purposes.
     *
     * @return A String representation of a drawCommand.
     */
    @Override
    public String toString() {
        return String.format("[%s] pen:%s, fill:%s, vertices:%s", this.shape, this.penColor, this.fillColor, Arrays.toString(this.vertices));
    }

    /**
     * Gets the shapeType of a drawCommand.
     *
     * @return ShapeType
     */
    public ShapeType getShape() {
        return shape;
    }

    /**
     * Gets the penColor of a drawCommand.
     *
     * @return Color (pen)
     */
    public Color getPenColor() {
        return penColor;
    }

    /**
     * Gets the fillColor of a drawCommand.
     *
     * @return Color (fill)
     */
    public Color getFillColor() {
        return fillColor;
    }

    /**
     * Gets the status of fill (enabled/disabled) of a drawCommand.
     *
     * @return boolean true is fill is enabled, otherwise false.
     */
    public boolean isFillEnabled() {
        return fillEnabled;
    }

    /**
     * Gets the vertices of a drawCommand.
     *
     * @return Float array of vertices.
     */
    public Float[] getVertices() {
        return vertices;
    }
}
