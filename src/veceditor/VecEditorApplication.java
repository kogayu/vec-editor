package veceditor;

/**
 * The main class that starts the application
 */
public class VecEditorApplication {
    public static void main(String[] args) {

        new MainFrame();

    }
}
