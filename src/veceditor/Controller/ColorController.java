package veceditor.Controller;

import veceditor.Model.ColorType;
import veceditor.Model.FillColorConfig;
import veceditor.Model.PenColorConfig;
import veceditor.View.ColorButton;
import veceditor.View.ColorButtonsPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controller class that reacts to events that happen on the colorButtonsPanel,
 * and calls appropriate methods from ColorController.
 */
public class ColorController {

    private FillColorConfig fillColorConfig;
    private PenColorConfig penColorConfig;
    private ColorButtonsPanel colorButtons;

    public ColorController(FillColorConfig fillColorConfig, PenColorConfig penColorConfig, ColorButtonsPanel colorButtons) {
        this.fillColorConfig = fillColorConfig;
        this.penColorConfig = penColorConfig;
        this.colorButtons = colorButtons;

        this.colorButtons.addActionListener(new ColorButtonListener());

    }

    private class ColorButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() instanceof ColorButton) {
                ColorButton source = (ColorButton) e.getSource();
                if (source == colorButtons.getFillColorButton()) {
                    // Display color selector dialog
                    Color newColor = JColorChooser.showDialog(null, "Choose a color", fillColorConfig.getCurrentColor());
                    // set selected color to fill color
                    setSelectedColor(newColor, ColorType.FILL);
                } else if (source == colorButtons.getPenColorButton()) {
                    // Display color selector dialog
                    Color newColor = JColorChooser.showDialog(null, "Choose a color", penColorConfig.getCurrentColor());
                    // set selected color to pen color
                    setSelectedColor(newColor, ColorType.PEN);
                }
            } else if (e.getSource() == colorButtons.getEnableFill()) {
                // toggle fill color config's enabled state
                fillColorConfig.setEnabled(!fillColorConfig.isEnabled());
            }
        }
    }

    /**
     * Set new color to indicated type
     *
     * @param newColor new color
     * @param type     type of color (pen or fill)
     */
    private void setSelectedColor(Color newColor, ColorType type) {
        // If user cancel to choose color...
        if (newColor == null) return;
        if (type == ColorType.FILL) {
            this.fillColorConfig.setCurrentColor(newColor);
            this.fillColorConfig.setEnabled(true); // enable fill color when color is selected.
        } else if (type == ColorType.PEN) {
            this.penColorConfig.setCurrentColor(newColor);
        }
    }


}
