package veceditor.Controller;

import veceditor.Model.ShapeConfig;
import veceditor.Model.ShapeType;
import veceditor.View.ShapeButton;
import veceditor.View.ShapeButtonsPanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Controller class that reacts to actions performed in the
 * ShapeButtonsPanel and call appropriate methods from ShapeConfig.
 */
public class ShapeController {

    private ShapeConfig shapeConfig; // model, subject

    private ShapeButtonsPanel shapeButtonsPanel; // view

    public ShapeController(ShapeConfig config, ShapeButtonsPanel shapeButtons) {
        this.shapeConfig = config;
        this.shapeButtonsPanel = shapeButtons;

        shapeButtons.addActionListener(new ShapeButtonListener());
    }

    private class ShapeButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            ShapeButton source = (ShapeButton) e.getSource();
            System.out.println("shape selected: " + e.getSource());
            if (source == shapeButtonsPanel.getPlotButton()) {
                shapeConfig.setCurrentShape(ShapeType.PLOT);
            } else if (source == shapeButtonsPanel.getLineButton()) {
                shapeConfig.setCurrentShape(ShapeType.LINE);
            } else if (source == shapeButtonsPanel.getRectangleButton()) {
                shapeConfig.setCurrentShape(ShapeType.RECTANGLE);
            } else if (source == shapeButtonsPanel.getEllipseButton()) {
                shapeConfig.setCurrentShape(ShapeType.ELLIPSE);
            } else if (source == shapeButtonsPanel.getPolygonButton()) {
                shapeConfig.setCurrentShape(ShapeType.POLYGON);
            }
        }
    }
}
