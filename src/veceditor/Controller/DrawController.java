package veceditor.Controller;

import veceditor.Model.DrawConfig;
import veceditor.View.CanvasPanel;

import java.awt.event.*;

/**
 * Controller class that reacts to events that happen on the CanvasPanel,
 * and calls appropriate methods from DrawConfig.
 */
public class DrawController {

    private DrawConfig config;

    private CanvasPanel canvas;

    public DrawController(DrawConfig config, CanvasPanel canvas) {
        this.config = config;
        this.canvas = canvas;

        // add listeners
        this.canvas.addMouseListener(new CanvasListener());
        this.canvas.addMouseMotionListener(new CanvasListener());
        this.canvas.setFocusable(true);
        this.canvas.addKeyListener(new CanvasListener());
    }

    public class CanvasListener implements MouseListener, MouseMotionListener, KeyListener {

        @Override
        public void mouseClicked(MouseEvent e) {

        }

        @Override
        public void mousePressed(MouseEvent e) {
            DrawController.this.config.processMousePress(e.getX(), e.getY());
            DrawController.this.canvas.repaint();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            DrawController.this.config.processMouseRelease(e.getX(), e.getY());
            DrawController.this.canvas.repaint();
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            DrawController.this.canvas.requestFocus();
        }

        @Override
        public void mouseExited(MouseEvent e) {
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            DrawController.this.config.processMouseDrag(e.getX(), e.getY());
            DrawController.this.canvas.repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            DrawController.this.config.processMouseMove(e.getX(), e.getY());
            if (DrawController.this.config.isDrawing()) {
                DrawController.this.canvas.repaint();
            }
        }

        @Override
        public void keyTyped(KeyEvent e) {

        }

        @Override
        public void keyPressed(KeyEvent e) {
            DrawController.this.config.processKeyPress(e.getKeyCode());
            DrawController.this.canvas.repaint();
        }

        @Override
        public void keyReleased(KeyEvent e) {

        }
    }
}
