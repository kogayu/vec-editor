package veceditor.Controller;

import veceditor.Exception.InvalidColorException;
import veceditor.Exception.InvalidCommandException;
import veceditor.Exception.InvalidImageSizeException;
import veceditor.Model.DrawConfig;
import veceditor.Model.FileConfig;
import veceditor.View.CanvasPanel;
import veceditor.View.TopMenuBar;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Controller class that reacts to events that happen when each JMenuItem in TopMenuBar is pressed,
 * and calls appropriate methods from TopMenuBarController.
 */
public class TopMenuBarController {
    private DrawConfig drawConfig;
    private FileConfig fileConfig;
    private CanvasPanel canvasPanel;
    private TopMenuBar topMenuBar;

    public TopMenuBarController(CanvasPanel canvasPanel, DrawConfig drawConfig, TopMenuBar topMenuBar) {
        this.drawConfig = drawConfig;
        this.fileConfig = new FileConfig(this.drawConfig);
        this.canvasPanel = canvasPanel;
        this.topMenuBar = topMenuBar;
        this.topMenuBar.addActionListener(new TopMenuBarListener());
    }

    /**
     * Define behaviors of JMenuItems in TopMenuBar
     */
    private class TopMenuBarListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            JMenuItem source = (JMenuItem) e.getSource();
            if (source == topMenuBar.getNewFile()) {
                fileConfig.setFile(null);
                drawConfig.getCommands().clear();
                canvasPanel.repaint();
            } else if (source == topMenuBar.getOpen()) {
                try {
                    fileConfig.drawCommandsFromFile(source);
                    canvasPanel.repaint();
                } catch (InvalidCommandException | IOException | InvalidColorException e1) {
                    e1.printStackTrace();
                }
            } else if (source == topMenuBar.getSave()) {
                try {
                    fileConfig.saveOnFile(source);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } else if (source == topMenuBar.getSaveAs()) {
                fileConfig.setFile(null);
                try {
                    fileConfig.saveOnFile(source);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } else if (source == topMenuBar.getBmpExport()) {
                try {
                    fileConfig.exportToBmp(source, canvasPanel);
                } catch (InvalidImageSizeException e2) {
                    e2.printStackTrace();
                }
            } else if (source == topMenuBar.getUndo()) {
                drawConfig.undo();
                canvasPanel.repaint();
            }
        }
    }


}
