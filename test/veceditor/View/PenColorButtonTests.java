package veceditor.View;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import veceditor.Model.PenColorConfig;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;
public class PenColorButtonTests {
    private PenColorConfig config;
    private ColorButton button;
    @BeforeEach
    public void setup(){
        this.config = new PenColorConfig();
        this.button = new ColorButton(this.config.getCurrentColor(), true);
        this.config.attachObserver(button);
    }
    @Test
    public void testDefaultBackgroundColor(){
        assertEquals(null,button.getBackground());
    }
    @Test void testUpdatedBackgroundColor(){
        this.config.setCurrentColor(Color.RED);
        assertEquals(Color.RED, this.button.getBackground());
    }
}

