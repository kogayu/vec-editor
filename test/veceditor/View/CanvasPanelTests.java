package veceditor.View;

import org.junit.jupiter.api.Test;
import veceditor.MainFrame;
import veceditor.Model.DrawConfig;
import veceditor.Model.FillColorConfig;
import veceditor.Model.PenColorConfig;
import veceditor.Model.ShapeConfig;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CanvasPanelTests {

    @Test
    public void testInitialize() {
        MainFrame frame = new MainFrame();
        ShapeConfig shapeConfig = new ShapeConfig();
        PenColorConfig penColorConfig = new PenColorConfig();
        FillColorConfig fillColorConfig = new FillColorConfig();
        DrawConfig config = new DrawConfig(shapeConfig, penColorConfig, fillColorConfig, 100);
        CanvasPanel panel = new CanvasPanel(frame, config);
        assertEquals(Color.WHITE, panel.getBackground());
    }
}
