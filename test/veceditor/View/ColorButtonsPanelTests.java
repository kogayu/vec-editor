package veceditor.View;

import org.junit.jupiter.api.Test;
import veceditor.Model.FillColorConfig;
import veceditor.Model.PenColorConfig;
import static org.junit.jupiter.api.Assertions.*;
public class ColorButtonsPanelTests {
    @Test
    public void testInitialize() {
        FillColorConfig fillColorConfig = new FillColorConfig();
        PenColorConfig penColorConfig = new PenColorConfig();
        ColorButtonsPanel colorButtonsPanel = new ColorButtonsPanel(fillColorConfig, penColorConfig);
        assertNotNull(colorButtonsPanel.getFillColorButton());
        assertNotNull(colorButtonsPanel.getPenColorButton());
        assertNotNull(colorButtonsPanel.getEnableFill());
    }
}
