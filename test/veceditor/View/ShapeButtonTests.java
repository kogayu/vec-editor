package veceditor.View;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import veceditor.Model.ShapeConfig;
import veceditor.Model.ShapeType;

import javax.swing.*;
import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class ShapeButtonTests {

    private ShapeConfig config;
    private ShapeButton button;

    @BeforeEach
    public void setup() {
        this.config = new ShapeConfig();
        Icon icon = new ImageIcon("../../../images/ellipse.png");
        this.button = new ShapeButton(icon, ShapeType.ELLIPSE);
        this.config.attachObserver(this.button);
    }

    @Test
    public void testSelectShapeButton() {
        this.config.setCurrentShape(ShapeType.ELLIPSE);
        assertEquals(Color.LIGHT_GRAY, this.button.getBackground());
    }

    @Test
    public void testUnselectShapeButton() {
        this.config.setCurrentShape(ShapeType.POLYGON);
        assertNull(this.button.getBackground());
    }
}
