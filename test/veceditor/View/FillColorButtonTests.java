package veceditor.View;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import veceditor.Model.FillColorConfig;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

public class FillColorButtonTests {
    private FillColorConfig config;
    private ColorButton button;

    @BeforeEach
    public void setup(){
        this.config = new FillColorConfig();
        this.button = new ColorButton(this.config.getCurrentColor(), true);
        this.config.attachObserver(button);
    }
    @Test
    public void testFillIsNotEnabledButtonLabelUpdated() {
        this.config.setEnabled(false);
        assertEquals("X", this.button.getButtonLabel());
    }
    @Test
    public void testFillIsEnabledButtonLabelUpdated(){
        this.config.setEnabled(true);
        assertNull(this.button.getButtonLabel());
    }
    @Test
    public void testFillIsNotEnabledBackgroundColorIsNull(){
        this.config.setEnabled(false);
        assertNull(this.button.getBackground());
    }
    @Test
    public void testFillIsNotEnabledAndBackgroundColorNull(){
        this.config.setEnabled(false);
        this.config.setCurrentColor(Color.BLUE);
        assertNull(this.button.getBackground());
    }
    @Test void testFillIsEnabledAndBackgroundColorUpdated(){
        this.config.setEnabled(true);
        this.config.setCurrentColor(Color.RED);
        assertEquals(Color.RED, this.button.getBackground());
    }

}
