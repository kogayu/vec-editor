package veceditor.View;

import org.junit.jupiter.api.Test;
import veceditor.Model.FillColorConfig;

import static org.junit.jupiter.api.Assertions.assertTrue;


public class FillCheckBoxTests {

    @Test
    public void testUpdate() {
        FillColorConfig config = new FillColorConfig();
        FillCheckBox checkBox = new FillCheckBox("fill");
        config.attachObserver(checkBox);
        config.setEnabled(true);
        assertTrue(checkBox.isSelected());
    }
}
