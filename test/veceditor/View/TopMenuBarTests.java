package veceditor.View;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TopMenuBarTests {
    @Test
    public void testInitialize() {
        TopMenuBar topMenuBar = new TopMenuBar();
        assertNotNull(topMenuBar.getNewFile());
        assertNotNull(topMenuBar.getOpen());
        assertNotNull(topMenuBar.getSave());
        assertNotNull(topMenuBar.getSaveAs());
        assertNotNull(topMenuBar.getBmpExport());
        assertNotNull(topMenuBar.getUndo());
    }
}
