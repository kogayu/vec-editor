package veceditor.View;

import org.junit.jupiter.api.Test;
import veceditor.Model.FillColorConfig;
import veceditor.Model.PenColorConfig;
import veceditor.Model.ShapeConfig;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SidePanelTests {

    @Test
    public void testInitialize() {
        ShapeConfig shapeConfig = new ShapeConfig();
        PenColorConfig penColorConfig = new PenColorConfig();
        FillColorConfig fillColorConfig = new FillColorConfig();
        SidePanel sidePanel = new SidePanel(10, 10, shapeConfig, fillColorConfig, penColorConfig);
        assertEquals(10, sidePanel.getPreferredSize().getWidth());
        assertEquals(10, sidePanel.getPreferredSize().getHeight());
    }
}
