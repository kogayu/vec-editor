package veceditor.View;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import veceditor.Model.ShapeConfig;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ShapeButtonsPanelTests {

    private ShapeButtonsPanel panel;
    private ShapeConfig config;

    @BeforeEach
    public void setup() {
        this.config = new ShapeConfig();
        this.panel = new ShapeButtonsPanel(this.config);
    }

    @Test
    public void testGetShapeButtons() {
        assertNotNull(this.panel.getPlotButton());
        assertNotNull(this.panel.getLineButton());
        assertNotNull(this.panel.getRectangleButton());
        assertNotNull(this.panel.getEllipseButton());
        assertNotNull(this.panel.getPolygonButton());
    }
}
