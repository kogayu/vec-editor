package veceditor.Model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShapeConfigTests {

    @Test
    public void testSetAndGetCurrentShape() {
        ShapeConfig config = new ShapeConfig();
        config.setCurrentShape(ShapeType.LINE);
        assertEquals(ShapeType.LINE, config.getCurrentShape());
    }
}
