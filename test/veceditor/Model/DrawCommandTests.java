package veceditor.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class DrawCommandTests {

    ArrayList<Float> coordinates = new ArrayList<>();
    private Float[] vertices = new Float[4];
    private DrawCommand drawCommand;

    @BeforeEach
    public void setup() {
        coordinates.add(0.1125f);
        coordinates.add(0.35714287f);
        coordinates.add(0.4642857f);
        coordinates.add(0.58928573f);
        vertices = coordinates.toArray(vertices);

        drawCommand = new DrawCommand(ShapeType.RECTANGLE, Color.BLACK, Color.BLACK, false, vertices);
    }

    @Test
    public void testGetShape() {
        assertEquals(ShapeType.RECTANGLE, drawCommand.getShape());
    }

    @Test
    public void testGetPenColor() {
        assertEquals(Color.BLACK, drawCommand.getPenColor());
    }

    @Test
    public void testGetFillColor() {
        assertEquals(Color.BLACK, drawCommand.getFillColor());
    }

    @Test
    public void testIsFillEnabled() {
        assertFalse(drawCommand.isFillEnabled());
    }

    @Test
    public void testGetVertices() {
        assertEquals(vertices, drawCommand.getVertices());
    }

    @Test
    public void testToString() {
        assertEquals(String.format("[%s] pen:%s, fill:%s, vertices:%s",
                ShapeType.RECTANGLE, Color.BLACK, Color.BLACK, Arrays.toString(this.vertices)), drawCommand.toString());
    }
}
