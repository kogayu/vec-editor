package veceditor.Model;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import veceditor.Exception.InvalidColorException;
import veceditor.Exception.InvalidCommandException;
import veceditor.Exception.InvalidImageSizeException;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class FileConfigTests {
    private ShapeConfig shapeConfig;
    private PenColorConfig penConfig;
    private FillColorConfig fillConfig;
    private DrawConfig drawConfig;
    private FileConfig fileConfig;
    private final Color defaultPenColor = Color.BLACK;
    private final Color defaultFillColor = null;
    private final boolean defaultIsFillEnabled = false;
    private ArrayList<DrawCommand> validCommands = new ArrayList<DrawCommand>(
            Arrays.asList(
                    new DrawCommand(ShapeType.LINE, Color.BLACK, null, false, new Float[]{0.1f, 0.2f, 0.3f, 0.4f}),
                    new DrawCommand(ShapeType.PLOT, Color.BLACK, null, false, new Float[]{0.4f, 0.1f, 0.4f, 0.1f}),
                    new DrawCommand(ShapeType.RECTANGLE, Color.BLACK, Color.BLUE, true, new Float[]{0.2f, 0.2f, 0.33f, 0.99f}),
                    new DrawCommand(ShapeType.ELLIPSE, Color.BLACK, Color.GREEN, true, new Float[]{0.15f, 0.01f, 0.2f, 0.23f}),
                    new DrawCommand(ShapeType.ELLIPSE, Color.BLACK, Color.GREEN, false, new Float[]{0.36f, 0.01f, 0.1f, 0.87f}),
                    new DrawCommand(ShapeType.POLYGON, Color.RED, Color.GREEN, true, new Float[]{0.16f, 0.11f, 0.14f, 0.17f, 0.23f, 0.56f})
            )
    );

    @BeforeEach
    public void setup() {
        this.shapeConfig = new ShapeConfig();
        this.penConfig = new PenColorConfig();
        this.fillConfig = new FillColorConfig();
        this.drawConfig = new DrawConfig(this.shapeConfig, this.penConfig, this.fillConfig, 10);
        this.fileConfig = new FileConfig(this.drawConfig);
        this.fileConfig.setTesting(true);
    }

    @AfterAll
    private static void deleteGenerated() {
        File testFile = new File("test/veceditor/Resources/test.vec");
        Boolean delete = testFile.delete();
        assertTrue(delete);
    }

    @Test
    public void testInitialization() {
        // file should be null
        assertNull(this.fileConfig.getFile());
        //FcForVec
        assertFalse(this.fileConfig.getFcForVec().isAcceptAllFileFilterUsed());
        assertEquals("VEC file", this.fileConfig.getFcForVec().getFileFilter().getDescription());

        //FcForBmp
        assertFalse(this.fileConfig.getFcForBmp().isAcceptAllFileFilterUsed());
        assertEquals("BMP file", this.fileConfig.getFcForBmp().getFileFilter().getDescription());

    }

    @Test
    public void testSetNewFile() {
        File newFile = new File("/test/Resources/newFile.vec");
        this.fileConfig.setFile(newFile);
        assertEquals(newFile, this.fileConfig.getFile());
    }

    @Test
    public void testNotExistingFile() {
        assertThrows(IOException.class, () -> {
            File notExistingFile = new File("/test/Resources/notExiting.vec");
            this.fileConfig.translateFileToCommands(notExistingFile);
        });
    }

    @Test
    public void testVecFileMisspellPLOT() {
        assertThrows(InvalidCommandException.class, () -> {
            this.fileConfig.translateFileToCommands(new File("test/veceditor/Resources/misspellPLOT.vec"));
        });
    }

    @Test
    public void testVecFileMisspellLINE() {
        assertThrows(InvalidCommandException.class, () -> {
            this.fileConfig.translateFileToCommands(new File("test/veceditor/Resources/misspellLINE.vec"));
        });
    }

    @Test
    public void testVecFileMisspellRECTANGLE() {
        assertThrows(InvalidCommandException.class, () -> {
            this.fileConfig.translateFileToCommands(new File("test/veceditor/Resources/misspellRECTANGLE.vec"));
        });
    }
    @Test
    public void testVecFileMisspellPOLYGON(){
        assertThrows(InvalidCommandException.class, () -> {
            this.fileConfig.translateFileToCommands(new File("test/veceditor/Resources/misspellPOLYGON.vec"));
        });
    }


    @Test
    public void testVecFileMisspellFILL() {
        assertThrows(InvalidCommandException.class, () -> {
            this.fileConfig.translateFileToCommands(new File("test/veceditor/Resources/misspellFILL.vec"));
        });
    }

    @Test
    public void testVecFileMisspellPEN() {
        assertThrows(InvalidCommandException.class, () -> {
            this.fileConfig.translateFileToCommands(new File("test/veceditor/Resources/misspellPEN.vec"));
        });
    }

    @Test
    public void testVecFileInvalidVertices() {
        assertThrows(InvalidCommandException.class, () -> {
            this.fileConfig.translateFileToCommands(new File("test/veceditor/Resources/invalidVertices.vec"));
        });
    }
    @Test
    public void testInvalidNumberOfVerticesForShapesExceptPOLYGON(){
        assertThrows(InvalidCommandException.class, () -> {
            this.fileConfig.translateFileToCommands(new File("test/veceditor/Resources/invalidNumberOfVerticesForShapesExceptPOLYGON.vec"));
        });
    }


    @Test
    public void testVecFileIsValid() throws InvalidCommandException, IOException, InvalidColorException {
        Float[] testVertices = {0.1f, 0.2f, 0.3f, 0.4f};
        ArrayList<DrawCommand> testCommands;
        DrawCommand testCommand = new DrawCommand(ShapeType.LINE, this.defaultPenColor, this.defaultFillColor, this.defaultIsFillEnabled, testVertices);
        testCommands = this.fileConfig.translateFileToCommands(new File("test/veceditor/Resources/success.vec"));
        assertEquals(testCommand.getShape(), testCommands.get(0).getShape());
        assertEquals(testCommand.getPenColor(), testCommands.get(0).getPenColor());
        assertEquals(testCommand.getFillColor(), testCommands.get(0).getFillColor());
        assertEquals(testCommand.isFillEnabled(), testCommands.get(0).isFillEnabled());
        for (int i = 0; i < testCommand.getVertices().length; i++) {
            assertEquals(testCommand.getVertices()[i], testCommands.get(0).getVertices()[i]);
        }
    }

    @Test
    public void testInvalidImageSizeInput() {
        assertThrows(InvalidImageSizeException.class, () ->
                this.fileConfig.convertStringToInt("hello")
        );
    }

    @Test
    public void testImageSizeIsNegativeNumber() {
        assertThrows(InvalidImageSizeException.class, () ->
                this.fileConfig.checkImageSize(-1)
        );
    }

    @Test
    public void testImageSizeIsOverMaximum() {
        assertThrows(InvalidImageSizeException.class, () ->
                this.fileConfig.checkImageSize(4097)
        );
    }

    @Test
    public void testImageSizeIsValid() throws InvalidImageSizeException {
        assertTrue(this.fileConfig.checkImageSize(1000));
    }


    @Test
    public void testMisspellOFF() {
        assertThrows(InvalidColorException.class, () ->
                this.fileConfig.checkFillOff("FILL OF")
        );
    }

    @Test
    public void testValidOFFSyntax() throws InvalidColorException {
        assertTrue(this.fileConfig.checkFillOff("FILL OFF"));
    }

    @Test
    public void testValidHexSyntax() throws InvalidColorException {
        assertFalse(this.fileConfig.checkFillOff("FILL #000000"));
    }

    //    add hex to color test
    @Test
    public void testHexDoesNotHaveSixDigits() {
        assertThrows(InvalidColorException.class, () -> {
            this.fileConfig.hexToColor("#00000");
        });
    }

    @Test
    public void testHexWithoutHash() {
        assertThrows(InvalidColorException.class, () ->
                this.fileConfig.hexToColor("000000")
        );
    }

    @Test
    public void testInvalidHexNum() {
        assertThrows(InvalidColorException.class, () ->
                this.fileConfig.hexToColor("#ZZ00ZZ")
        );
    }

    @Test
    public void testValidHexColor() throws InvalidColorException {
        String testHexColor = "#FFAAFF";
        Color testColor = new Color(255, 170, 255);
        Color resultColor = this.fileConfig.hexToColor(testHexColor);
        assertEquals(testColor.getRed(), resultColor.getRed());
        assertEquals(testColor.getGreen(), resultColor.getGreen());
        assertEquals(testColor.getBlue(), resultColor.getBlue());
    }

    // add writeCommandsOnFile test
    // Failed commands
    @Test
    public void testInvalidCommands() throws InvalidCommandException {
        assertThrows(InvalidCommandException.class, () -> {
            ArrayList<DrawCommand> invalidCommands = new ArrayList<DrawCommand>(
                    Arrays.asList(
                            new DrawCommand(ShapeType.LINE, Color.BLACK, null, false, new Float[]{1.1f, 2.2f, 0.3f, 0.4f}),
                            new DrawCommand(ShapeType.PLOT, Color.BLUE, null, false, new Float[]{0.4f, 1.1f, 0.4f, 0.1f}),
                            new DrawCommand(ShapeType.RECTANGLE, Color.BLACK, Color.BLUE, true, new Float[]{0.2f, 0.2f, 9.33f, 0.99f}),
                            new DrawCommand(ShapeType.ELLIPSE, Color.BLACK, Color.GREEN, true, new Float[]{0.15f, 1.01f, 0.2f, 0.23f}),
                            new DrawCommand(ShapeType.ELLIPSE, Color.BLACK, Color.GREEN, false, new Float[]{0.36f, 5.01f, 0.1f, 0.87f}),
                            new DrawCommand(ShapeType.POLYGON, Color.YELLOW, Color.BLUE, true, new Float[]{0.13f, 5.21f, 1.1f, 0.823f})
                    )
            );
            File testFile = new File("test/veceditor/Resources/test.vec");
            this.fileConfig.writeCommandsOnFile(invalidCommands, testFile);
            this.fileConfig.translateFileToCommands(testFile);
        });
    }

    @Test
    public void testInvalidFilePath() {
        File notExistingFile = new File("/notExistingFile.vec");
        assertThrows(IOException.class, () -> {
            this.fileConfig.writeCommandsOnFile(this.validCommands, notExistingFile);
        });
    }

    @Test
    public void testValidFilePathAndDrawCommands() throws InvalidCommandException, IOException, InvalidColorException {
        File testFile = new File("test/veceditor/Resources/test.vec");
        this.fileConfig.writeCommandsOnFile(this.validCommands, testFile);
        ArrayList<DrawCommand> commandsOnSavedFile = this.fileConfig.translateFileToCommands(testFile);
        for (int i = 0; i < commandsOnSavedFile.size(); i++) {
            assertEquals(this.validCommands.get(i).getShape(), commandsOnSavedFile.get(i).getShape());
            assertEquals(this.validCommands.get(i).getPenColor(), commandsOnSavedFile.get(i).getPenColor());
            assertEquals(this.validCommands.get(i).getFillColor(), commandsOnSavedFile.get(i).getFillColor());
            assertEquals(this.validCommands.get(i).isFillEnabled(), commandsOnSavedFile.get(i).isFillEnabled());
            for (int j = 0; j < this.validCommands.get(i).getVertices()[j]; j++) {
                assertEquals(this.validCommands.get(i).getVertices()[j], commandsOnSavedFile.get(i).getVertices()[j]);
            }
        }
    }

}
