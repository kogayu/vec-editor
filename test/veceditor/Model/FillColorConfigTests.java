package veceditor.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

public class FillColorConfigTests {
    private FillColorConfig config;

    @BeforeEach
    public void setup(){
        this.config = new FillColorConfig();
    }

    @Test
    public void testDefaultEnableIsFalse(){
        assertFalse(this.config.isEnabled());
    }
    @Test
    public void testDefaultFillColor(){
        assertEquals(Color.BLACK, this.config.getCurrentColor());
    }
    @Test
    public void testSetNewEnabledState(){
        this.config.setEnabled(true);
        assertTrue(this.config.isEnabled());
    }
    @Test
    public void testSetNewColor(){
        this.config.setCurrentColor(Color.RED);
        assertEquals(Color.RED, this.config.getCurrentColor());
    }
}
