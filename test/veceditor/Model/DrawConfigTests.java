package veceditor.Model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class DrawConfigTests {

    private ShapeConfig shapeConfig;
    private PenColorConfig penConfig;
    private FillColorConfig fillConfig;
    private DrawConfig config;

    @BeforeEach
    public void setup() {
        this.shapeConfig = new ShapeConfig();
        this.penConfig = new PenColorConfig();
        this.fillConfig = new FillColorConfig();
        this.config = new DrawConfig(this.shapeConfig, this.penConfig, this.fillConfig, 10);
        this.shapeConfig.attachObserver(this.config);
        this.penConfig.attachObserver(this.config);
        this.fillConfig.attachObserver(this.config);
        this.penConfig.setCurrentColor(Color.BLACK);
        this.fillConfig.setCurrentColor(Color.BLACK);
        this.fillConfig.setEnabled(false);
    }

    @Test
    public void testSetAndGetCanvasLength() {
        this.config.setCanvasLength(100);
        assertEquals(100, this.config.getCanvasLength());
    }

    @Test
    public void testSetAndGetCommands() {
        ArrayList<DrawCommand> commands = new ArrayList<DrawCommand>();
        commands.add(new DrawCommand(ShapeType.LINE, Color.BLACK, Color.BLACK, false, new Float[]{1f, 2f}));
        this.config.setCommands(commands);
        assertEquals(commands, this.config.getCommands());
    }

    @Test
    public void testProcessMousePressNonPlot() {
        this.shapeConfig.setCurrentShape(ShapeType.LINE);
        this.config.processMousePress(0, 0);
        assertTrue(this.config.isDrawing());
        assertEquals(2, this.config.getCoordinates().size());
    }

    @Test
    public void testProcessMousePressPlot() {
        this.shapeConfig.setCurrentShape(ShapeType.PLOT);
        this.config.processMousePress(0, 0);
        assertFalse(this.config.isDrawing());
        assertEquals(1, this.config.getCommands().size());
    }

    @Test
    public void testProcessMouseRelease() {
        this.shapeConfig.setCurrentShape(ShapeType.LINE);
        this.config.processMousePress(0, 0);
        this.config.processMouseDrag(1, 1);
        this.config.processMouseRelease(0, 0);
        assertFalse(this.config.isDrawing());
        assertEquals(1, this.config.getCommands().size());
    }

    @Test
    public void testProcessMouseReleaseEnd() {
        this.shapeConfig.setCurrentShape(ShapeType.LINE);
        this.config.processMousePress(0, 0);
        this.config.processMouseDrag(1, 1);
        this.config.processMouseRelease(2, 2);
        assertEquals(1, this.config.getCommands().size());
    }

    @Test
    public void testProcessMouseDrag() {
        this.shapeConfig.setCurrentShape(ShapeType.LINE);
        this.config.processMousePress(0, 0);
        this.config.processMouseDrag(0, 0);
        assertEquals(4, this.config.getCoordinates().size());
    }

    @Test
    public void testProcessMouseDrag2() {
        this.shapeConfig.setCurrentShape(ShapeType.LINE);
        this.config.processMousePress(0, 0);
        this.config.processMouseDrag(1, 1);
        this.config.processMouseDrag(2, 2);
        assertEquals(4, this.config.getCoordinates().size());
    }

    @Test
    public void testProcessMouseMove() {
        this.shapeConfig.setCurrentShape(ShapeType.POLYGON);
        this.config.processMousePress(0, 0);
        this.config.processMouseMove(0, 0);
        assertEquals(4, this.config.getCoordinates().size());
    }

    @Test
    public void testProcessMouseMove2() {
        this.shapeConfig.setCurrentShape(ShapeType.POLYGON);
        this.config.processMousePress(0, 0);
        this.config.processMouseMove(0, 1);
        this.config.processMouseMove(1, 0);
        assertEquals(4, this.config.getCoordinates().size());
    }

    @Test
    public void testProcessSpacePress() {
        this.shapeConfig.setCurrentShape(ShapeType.POLYGON);
        this.config.processMousePress(0, 0);
        this.config.processKeyPress(32);
        assertFalse(this.config.isDrawing());
        assertEquals(1, this.config.getCommands().size());
    }

    @Test
    public void testProcessSpacePress2() {
        this.shapeConfig.setCurrentShape(ShapeType.POLYGON);
        this.config.processMousePress(0, 0);
        this.config.processMouseMove(0, 1);
        this.config.processMouseMove(1, 0);
        this.config.processKeyPress(32);
        assertFalse(this.config.isDrawing());
        assertEquals(1, this.config.getCommands().size());
        assertEquals(0, this.config.getCoordinates().size());
    }

    @Test
    public void testProcessUndoPress() {
        this.shapeConfig.setCurrentShape(ShapeType.POLYGON);
        this.config.processMousePress(0, 0);
        this.config.processKeyPress(32);
        this.config.processKeyPress(90);
        assertEquals(0, this.config.getCommands().size());
    }
}
